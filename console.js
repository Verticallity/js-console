var helpPages =
[
	[
		"HELP: Page 1 - Meta",
		"The 'help' command can be followed by a number for a specific help page",
		"The 'help' command can also be used with a word for help on specific commands or topics",
		"The console can be opened and closed with [TAB]"
	],
	[
		"HELP: Page 2 - About",
		"This is a console for some of my projects, you can use it if you want",
		"but I really have no idea why you would, it took me 2 days."
	]
];
var helpLookup =
{
	help : 
		[
			"HELP: The 'help' command",
			"The 'help' command takes 0 or 1 arguments",
			"With 0 arguments, the first help page is output",
			"One argument can either be used as a page number or a keyword"
		]
}

var commands =
{
	clear :
		function(arguments) {
			for (var i = 0; i < NUM_OF_CONSOLE_LINES; i++) {
				consoleLines[i] = "";
				textConsole.children[i].textContent = "";
			}
			signalNote();
			return 0;
		},
	
	open :
		function(arguments) {
			consoleShown = true;
			textConsole.parentElement.className = "full";
			return 0;
		},
	show :
		function(arguments) {
			return commands.open(arguments);
		},
	
	close :
		function(arguments) {
			consoleShown = false;
			textConsole.parentElement.className = "";
			return 0;
		},
	hide :
		function(arguments) {
			return commands.close(arguments);
		},
	
	
	help :
		function(arguments) {
			if (arguments.length === 0) {
				writeMultipleToConsole(helpPages[0]);
			} else if (isNaN(arguments[0])) {
				if (helpLookup[arguments[0]] != undefined) {
					writeMultipleToConsole(helpLookup[arguments[0]]);
				} else {
					return "Invalid help topic";
				}
			} else {
				if (arguments[0] > 0 && arguments[0] <= helpPages.length) {
					writeMultipleToConsole(helpPages[arguments[0]-1]);
				} else {
					return "Invalid help page number";
				}
			}
			consoleShown = true;
			textConsole.parentElement.className = "full";
			return 0;
		}
};
function getRune(curIn) {
	return 0;
};
var previewCommands =
{
	always : function(parts) {
		return "";
	},
	regular : function(parts) {
		return "";
	}
};
var inlineFunctions = [];



var inputBar;
var currentInput;
var storedCommand;
var pastCommands;
var commandsUp;

var cursorPosition;

var NUM_OF_CONSOLE_LINES = 16;
var textConsole;
var consoleShown = false;
var consoleLines;

var sinceLastDisplay;

function doConsoleSetup() {
	inputBar = document.getElementById('comminput');
	currentInput = "";
	storedCommand = "";
	pastCommands = [];
	commandsUp = 0;
	
	cursorPosition = 0;
	
	textConsole = document.getElementById('commconsole');
	consoleLines = [];
	for (var i = 0; i < NUM_OF_CONSOLE_LINES; i++) {
		consoleLines.push("");
		textConsole.innerHTML += "<p class=\"consoleline\"></p>";
	}
	
	sinceLastDisplay = 0;
	
	window.addEventListener('keydown', function(event) {
		if (event.isComposing || event.altKey || event.ctrlKey || event.metaKey) {
			return;
		}
		
		event.preventDefault();
		if (event.key.length === 1) {
			currentInput = currentInput.substr(0, cursorPosition) + event.key + currentInput.substr(cursorPosition);
			cursorPosition++;
	
			repInlines();
		} else {
			switch (event.key) {
			case "Enter":
				if (!currentInput.match(/^\s*$/)) {
		   			sendCommand(currentInput);
				} else {
					// Also handled in sendCommand() to allow for commands to deposit stuff in currentInput
					currentInput = "";
					storedCommand = "";
					commandsUp = 0;
					cursorPosition = 0;
				}
				
				break;
		
			case "ArrowUp":
				if (commandsUp < pastCommands.length) {
					if (commandsUp === 0) {
						storedCommand = currentInput;
					}
					commandsUp++;
					currentInput = pastCommands[pastCommands.length - commandsUp];
					cursorPosition = currentInput.length;
				}
			
				event.preventDefault();
				break;
			case "ArrowDown":
				if (commandsUp > 0) {
					commandsUp--;
			
					currentInput = ((commandsUp === 0) ? (storedCommand) : (pastCommands[pastCommands.length - commandsUp]));
				}
				cursorPosition = currentInput.length;
			
				event.preventDefault();
				break;
			case "ArrowLeft":
				if (cursorPosition > 0) {
					cursorPosition--;
				}
			
				event.preventDefault();
				break;
			case "ArrowRight":
				if (cursorPosition < currentInput.length) {
					cursorPosition++;
				}
			
				event.preventDefault();
				break;
			
			case "Home":
				cursorPosition = 0;
				break;
			
			case "End":
				cursorPosition = currentInput.length;
				break;
		
			case "Tab":
				consoleShown = !consoleShown;
				textConsole.parentElement.className = ((consoleShown) ? ("full") : (""));
				event.preventDefault();
				break;
		
			case "Backspace":
				currentInput = currentInput.substr(0, cursorPosition-1) + currentInput.substr(cursorPosition);
				if (cursorPosition != 0) {
					cursorPosition--;
				}
			
				repInlines();
				break;
			case "Delete" :
				currentInput = currentInput.substr(0, cursorPosition) + currentInput.substr(cursorPosition+1);
			
				repInlines();
				break;
			case "Clear":
				currentInput = "";
				cursorPosition = 0;
			
				repInlines();
				break;
			default:
				break;
			}
		}
		
		updateTextPrompt();
	});
}

function updateTextPrompt() {
	var currentRune = getRune(currentInput);
	currentRune = ((currentRune === 0) ? ("&gt;") : (escapeHtml(currentRune)));
	
	var commandPreview = getCommandPreview(currentInput);
	
	if (cursorPosition === currentInput.length) {
		inputBar.innerHTML = currentRune + "&nbsp;" + escapeHtml(currentInput).replace(/\s/g,"&nbsp;") + "<div id=\"texthighlight\">&nbsp;</div>&nbsp;&nbsp;<csgr>" + commandPreview + "</csgr>";
	} else {
		inputBar.innerHTML = currentRune + "&nbsp;" + escapeHtml(currentInput.substr(0,cursorPosition)).replace(/\s/g,"&nbsp;") + "<div id=\"texthighlight\">" + escapeHtml(currentInput.substr(cursorPosition,1)).replace(/\s/g,"&nbsp;") + "</div>" + escapeHtml(currentInput.substr(cursorPosition+1)).replace(/\s/g,'&nbsp;') + "&nbsp;&nbsp;&nbsp;<csgr>" + commandPreview + "</csgr>";
	}
}



function sendCommand(comm) {
	pastCommands.push(comm);
	var commandPartsTemp = comm.split(" ");
	var commandParts = [];
	for (var i = 0; i < commandPartsTemp.length; i++) {
		if (commandPartsTemp[i] != "") {
			commandParts.push(commandPartsTemp[i]);
		}
	}
	
	// Handled now to allow for deposition into currentInput
	currentInput = "";
	storedCommand = "";
	commandsUp = 0;
	cursorPosition = 0;
	
	var command = commandParts.shift();
	if (commands[command] != undefined) {
		sinceLastDisplay = 0;
		writeToConsole("<csgr>&gt; "+comm+"</csgr>");
		var commandError = commands[command](commandParts);
		if (commandError != 0) {
			writeToConsoleSpecial("error", commandError);
			
			var currentLocation = NUM_OF_CONSOLE_LINES - sinceLastDisplay;
			if (currentLocation >= 0) {
				consoleLines[currentLocation] = consoleLines[currentLocation].replace("csgr","csred");
				textConsole.children[currentLocation].innerHTML = consoleLines[currentLocation];
			}
		}
	} else {
		writeToConsole("<csred>&gt; "+comm+"</csred>");
		writeToConsoleSpecial("error", "Unknown command");
	}
}

function getCommandPreview(comm) {
	if (comm.match(/^\s*$/)) {
		var defaultPrev = previewCommands.always([]);
		return (defaultPrev + previewCommands.regular([]));
	}
	
	var commandPartsTemp = comm.split(" ");
	var commandParts = [];
	for (var i = 0; i < commandPartsTemp.length; i++) {
		if (commandPartsTemp[i] != "") {
			commandParts.push(commandPartsTemp[i]);
		}
	}
	
	var command = commandParts.shift();
	var defaultPrev = previewCommands.always(commandParts);
	
	if (previewCommands[command] != undefined) {
		return (defaultPrev + previewCommands[command](commandParts));
	}
	
	commandParts.unshift(command);
	return (defaultPrev + previewCommands.regular(commandParts));
}





function signalError() {
	if (!consoleShown) {
		inputBar.className += " erroralert";
		setTimeout((function(){
			inputBar.className=inputBar.className += " endalert";
			setTimeout((function(){
				inputBar.className=inputBar.className.replace(/ \S*?alert/g, "");
			}), 300);
		}), 300);
	}
}
function signalNote() {
	if (!consoleShown) {
		inputBar.className += " notealert";
		setTimeout((function(){
			inputBar.className=inputBar.className += " endalert";
			setTimeout((function(){
				inputBar.className=inputBar.className.replace(/\s\S*?alert/g, "");
			}), 300);
		}), 300);
	}
}





function repInlines() {
	var emptyMat = currentInput.match(/\[\]/);
	while (emptyMat) {
		currentInput = currentInput.replace(/\[\]/, "");
		cursorPosition = emptyMat.index;
		emptyMat = currentInput.match(/\[\]/);
	}
	
	var currMatch = currentInput.match(/~\(([^~[]*?)\)|~([^~([][^~[]*?)\s|~(\s)|\[([^~[]+?)\]/);
	var failedMatches = [];
	var failedMathematics = [];
	
	while (currMatch) {
		if (currMatch[4] === undefined) {
			if (currMatch[3] === undefined) {
				if (currMatch[2] === undefined) {
					var ilArgs = currMatch[1].split(',');
				} else {
					var ilArgs = currMatch[2].split(',');
				}
			} else {
				var ilArgs = [];
			}
			test1 = ilArgs;
			if (ilArgs.length < inlineFunctions.length) {
				if (ilArgs.length === 1 && ilArgs[0] === "") {
					ilArgs = [];
				}
				
				var funcGet = inlineFunctions[ilArgs.length](ilArgs);
				
				if (funcGet != -1) {
					currentInput = currentInput.replace(/~\([^~]*?\)|~[^~(][^~]*?\s|~\s/, funcGet);
					cursorPosition = currMatch.index + (""+funcGet).length;
					
					for (var i = 0; i < failedMatches.length; i++) {
						failedMatches[i].index += funcGet - currMatch[0].length;
						failedMathematics[i].index += funcGet - currMatch[0].length;
					}
				} else {
					currentInput = currentInput.replace(/~\([^~]*?\)|~[^~(][^~]*?\s|~\s/, "");
					failedMatches.unshift(currMatch);
				}
			} else {
				currentInput = currentInput.replace(/~\([^~]*?\)|~[^~(][^~]*?\s|~\s/, "");
				failedMatches.unshift(currMatch);
			}
		} else {
			var matOut = doMathematics(currMatch[4]);
			
			if (!isNaN(matOut)) {
				matOut = (+matOut).toFixed(3).replace(/\.?0+$/, "");
				currentInput = currentInput.replace(/\[[^~[]+?\]/, matOut);
				cursorPosition = currMatch.index + matOut.length;	
				
				for (var i = 0; i < failedMatches.length; i++) {
					failedMatches[i].index += matOut - currMatch[0].length;
					failedMathematics[i].index += matOut - currMatch[0].length;
				}
			} else {
				currentInput = currentInput.replace(/\[[^~[]+?\]/, "[]");
				failedMathematics.unshift(currMatch);
			}
		}
			
		currMatch = currentInput.match(/~\(([^~[]*?)\)|~([^~([][^~[]*?)\s|~(\s)|\[([^~[]+?)\]/);
	}
		
	for (var i = 0; i < failedMatches.length; i++) {
		var iM = failedMatches[i];
		currentInput = currentInput.substr(0,iM.index) + iM[0] + currentInput.substr(iM.index);
	}
	
	for (var i = 0; i < failedMathematics.length; i++) {
		var iM = failedMathematics[i];
		currentInput = currentInput.substr(0,iM.index) + iM[0] + currentInput.substr(iM.index+2);
	}
}

function doMathematics(inStr) {
	if (inStr === "") {
		return "";
	}
	if (inStr.match(/^\d*(\d|\.\d+)$/)) {
		return ("" + Math.round(+inStr));
	}
	
	var workingStr = "[" + inStr + "]";
	
	var curMatch = workingStr.match(/\b(-?\d*(\d|\.\d+))\s*\^\s*(-?\d*(\d|\.\d+))(\s*[^^])/);
	while (curMatch) {
		workingStr = workingStr.replace(/\b-?\d*(\d|\.\d+)\s*\^\s*-?\d*(\d|\.\d+)\s*[^^]/, ((+curMatch[1]) ** (+curMatch[3])) + "" + curMatch[5]);
		
		curMatch = workingStr.match(/\b(-?\d*(\d|\.\d+))\s*\^\s*(-?\d*(\d|\.\d+))(\s*[^^])/);
	}
	
	curMatch = workingStr.match(/\b(-?\d*(\d|\.\d+))([*/])(-?\d*(\d|\.\d+))\b/);
	while (curMatch) {
		workingStr = workingStr.replace(/\b-?\d*(\d|\.\d+)[*/]-?\d*(\d|\.\d+)\b/, ((curMatch[3] === "*") ? ((+curMatch[1]) * (+curMatch[4])) : ((+curMatch[1]) / (+curMatch[4]))));
		
		curMatch = workingStr.match(/\b(-?\d*(\d|\.\d+))([*/])(-?\d*(\d|\.\d+))\b/);
	}
	
	curMatch = workingStr.match(/\b(-?\d*(\d|\.\d+))([+-])(-?\d*(\d|\.\d+))\b/);
	while (curMatch) {
		workingStr = workingStr.replace(/\b-?\d*(\d|\.\d+)[+-]-?\d*(\d|\.\d+)\b/, ((curMatch[3] === "+") ? ((+curMatch[1]) + (+curMatch[4])) : ((+curMatch[1]) - (+curMatch[4]))));
		console.log([curMatch[1], curMatch[2], curMatch[3]]);
		curMatch = workingStr.match(/\b(-?\d*(\d|\.\d+))([+-])(-?\d*(\d|\.\d+))\b/);
	}
	
	return workingStr.substr(1,workingStr.length-2);
}





var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}





function writeToConsole(text) {
	for (var i = 0; i < NUM_OF_CONSOLE_LINES; i++) {
		consoleLines[i] = ((i===NUM_OF_CONSOLE_LINES-1) ? (text) : (consoleLines[i+1]));
		
		if (consoleLines[i].match("<")) {
			textConsole.children[i].innerHTML = consoleLines[i];
		} else {
			textConsole.children[i].textContent = consoleLines[i];
		}
	}
	sinceLastDisplay++;
}

function writeToConsoleSpecial(type, text) {
	switch (type) {
		case "error":
			var start = "<cserror>ERROR: </cserror>";
			var end = "";
			signalError();
			
			break;
		case "note":
			var start = "<csnote>NOTE: </csnote>";
			var end = "";
			signalNote();
			
			break;
		default:
			var start = "";
			var end = "";
			break;
	}
	
	for (var i = 0; i < NUM_OF_CONSOLE_LINES; i++) {
		consoleLines[i] = ((i===NUM_OF_CONSOLE_LINES-1) ? (start+text+end) : (consoleLines[i+1]));
		
		if (consoleLines[i].match("<")) {
			textConsole.children[i].innerHTML = consoleLines[i];
		} else {
			textConsole.children[i].textContent = consoleLines[i];
		}
	}
	sinceLastDisplay++;
}

function writeMultipleToConsole(texts) {
	for (var i = 0; i < NUM_OF_CONSOLE_LINES; i++) {
		consoleLines[i] = ((i >= NUM_OF_CONSOLE_LINES - texts.length) ? (texts[i - NUM_OF_CONSOLE_LINES + texts.length]) : (consoleLines[i+texts.length]));
		
		if (consoleLines[i].match("<")) {
			textConsole.children[i].innerHTML = consoleLines[i];
		} else {
			textConsole.children[i].textContent = consoleLines[i];
		}
	}
	sinceLastDisplay += texts.length;
}

/*
Alternate console, doesn't scroll well, but allows it

function writeToConsole(text) {
	textConsole.innerHTML += "<p class=\"consoleline\">" + text + "</p>";
}

function writeMultipleToConsole(texts) {
	for (var i = 0; i < texts.length; i++) {
		textConsole.innerHTML += "<p class=\"consoleline\">" + texts[i] + "</p>";
	}
}*/
